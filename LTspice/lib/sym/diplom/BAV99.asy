Version 4
SymbolType CELL
LINE Normal -6 -48 -37 -48
LINE Normal 27 -48 58 -48
LINE Normal -6 -60 -6 -37
LINE Normal 11 -48 -6 -60
LINE Normal -6 -37 11 -48
LINE Normal 10 -60 10 -37
LINE Normal 11 -48 27 -48
LINE Normal -38 -48 -96 -48
LINE Normal -37 -48 -38 -48
LINE Normal 80 -48 58 -48
LINE Normal 14 48 45 48
LINE Normal -19 48 -50 48
LINE Normal 14 36 14 59
LINE Normal -3 48 14 36
LINE Normal 14 59 -3 48
LINE Normal -2 36 -2 59
LINE Normal -3 48 -19 48
LINE Normal 45 48 46 48
LINE Normal -68 48 -50 48
LINE Normal 80 48 46 48
LINE Normal 80 -48 80 48
LINE Normal 96 0 80 0
LINE Normal -96 48 -68 48
RECTANGLE Normal -97 -64 95 64
WINDOW 38 3 18 Center 2
SYMATTR SpiceModel BAV99
SYMATTR Prefix X
SYMATTR Description High Density Mounting Type Photocoupler
SYMATTR ModelFile TLP183.mod
PIN -96 -48 LEFT 5
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 96 0 RIGHT 5
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN -96 48 LEFT 5
PINATTR PinName 2
PINATTR SpiceOrder 2
