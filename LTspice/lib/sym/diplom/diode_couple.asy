Version 4
SymbolType BLOCK
PIN -16 -32 RIGHT 8
PINATTR PinName anode1
PINATTR SpiceOrder 1
PIN 16 -32 LEFT 8
PINATTR PinName cathode1
PINATTR SpiceOrder 2
PIN -16 32 RIGHT 8
PINATTR PinName anode2
PINATTR SpiceOrder 4
PIN 16 32 LEFT 8
PINATTR PinName cathode2
PINATTR SpiceOrder 3
