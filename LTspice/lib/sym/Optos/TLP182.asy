Version 4
SymbolType CELL
LINE Normal 48 -28 48 28
LINE Normal 96 48 80 48
LINE Normal 80 28 64 20
LINE Normal 80 28 72 12
LINE Normal 64 20 72 12
LINE Normal -64 -16 -64 -47
LINE Normal -64 17 -64 48
LINE Normal 48 4 68 16
LINE Normal 79 49 79 28
LINE Normal 80 -31 48 -5
LINE Normal 80 -47 80 -31
LINE Normal 95 -47 80 -47
LINE Normal -52 -16 -75 -16
LINE Normal -64 1 -52 -16
LINE Normal -75 -16 -64 1
LINE Normal -52 0 -75 0
LINE Normal -64 1 -64 17
LINE Normal -64 -48 -96 -48
LINE Normal -64 -47 -64 -48
LINE Normal -64 48 -96 48
LINE Normal 22 -8 -10 -8
LINE Normal -1 0 22 -8
LINE Normal 39 0 -1 0
LINE Normal 29 3 39 0
LINE Normal 29 -4 39 0
LINE Normal -44 0 -21 0
LINE Normal -32 -17 -44 0
LINE Normal -21 0 -32 -17
LINE Normal -44 -16 -21 -16
LINE Normal -32 -48 -64 -48
LINE Normal -32 -16 -32 -48
LINE Normal -32 -16 -32 -16
LINE Normal -32 48 -64 48
LINE Normal -32 0 -32 48
RECTANGLE Normal -97 -64 95 64
CIRCLE Normal -63 -47 -66 -50
CIRCLE Normal -62 50 -66 46
WINDOW 0 0 -80 Center 2
WINDOW 3 0 80 Center 2
SYMATTR Value TLP182
SYMATTR Prefix X
SYMATTR SpiceModel TLP182
SYMATTR Description AC-input Photocoupler
SYMATTR ModelFile TLP182.mod
PIN -96 -48 LEFT 5
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -96 48 LEFT 5
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 96 48 RIGHT 5
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 96 -48 RIGHT 5
PINATTR PinName 4
PINATTR SpiceOrder 4
